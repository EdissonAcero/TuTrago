<?php
    require "persistencia/Conexion.php";
    require "persistencia/ClienteDAO.php";

    class Cliente {
        
        private $idUsuario;
        private $nombre;
        private $apellido;
        private $direccion;
        private $NIT_CC;
        private $correo;
        private $clave;
        private $idTelefono;
        private $conexion;
        private $clienteDAO;
        
        public function getIdUsuario()
        {
            return $this->idUsuario;
        }
    
        public function getNombre()
        {
            return $this->nombre;
        }
    
        public function getApellido()
        {
            return $this->apellido;
        }
    
        public function getDireccion()
        {
            return $this->direccion;
        }
    
        public function getNIT_CC()
        {
            return $this->NIT_CC;
        }
    
        public function getCorreo()
        {
            return $this->correo;
        }
    
        public function getClave()
        {
            return $this->clave;
        }
    
        public function getIdTelefono()
        {
            return $this->idTelefono;
        }
    
        public function setIdUsuario($idUsuario)
        {
            $this->idUsuario = $idUsuario;
        }
    
        public function setNombre($nombre)
        {
            $this->nombre = $nombre;
        }
    
        public function setApellido($apellido)
        {
            $this->apellido = $apellido;
        }
    
        public function setDireccion($direccion)
        {
            $this->direccion = $direccion;
        }
    
        public function setNIT_CC($NIT_CC)
        {
            $this->NIT_CC = $NIT_CC;
        }
    
        public function setCorreo($correo)
        {
            $this->correo = $correo;
        }
    
        public function setClave($clave)
        {
            $this->clave = $clave;
        }
    
        public function setIdTelefono($idTelefono)
        {
            $this->idTelefono = $idTelefono;
        }
    
        function Cliente($pidUsuario = "", $pNombre = "", $pApellido = "", $pDireccion = "", $pNIT_CC = "", $pCorreo = "", $pClave = "", $pidTelefono = "") {
            $this -> idUsuario = $pidUsuario;
            $this -> nombre = $pNombre;
            $this -> apellido = $pApellido;
            $this -> NIT_CC = $pNIT_CC;
            $this -> correo = $pCorreo;
            $this -> clave = $pClave;
            $this -> idTelefono = $pidTelefono;
            $this -> conexion = new Conexion();
            $this -> clienteDAO = new ClienteDAO($pidUsuario, $pNombre, $pApellido, $pDireccion, $pNIT_CC, $pCorreo, $pClave, $pidTelefono);        
        }
        
        function autenticar() {
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> clienteDAO -> autenticar());
            $this -> conexion -> cerrar();
            /*validar que solo sea un cliente por la conexion*/
            if ($this -> conexion -> numFilas() == 1) {
                $resultado = $this -> conexion -> extraer();
                $this -> idCliente = $resultado[0];
                return true;
            }else {
                return false;
            }            
        }
    }
?>    