<?php
    require "logica/Cliente.php";
    
    $pid = null;
    
    if (isset($_GET["pid"])) {
        $pid = base64_decode($_GET["pid"]);
    }
?>

<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
        <link href="https://bootswatch.com/4/lux/bootstrap.css" rel="stylesheet" />	
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        
        <title>TuTrago</title>
        
        <link rel="icon" type="image/png" href="img/logoli.png">
        
        <script type="text/javascript">
        	$(function () {
        	  $('[data-toggle="tooltip"]').tooltip()
        	})
    	</script>
    </head>
    <body>
        <?php
            if (isset($pid)) {
                include $pid;
            }else {
                include "presentacion/Inicio.php";
            }
        ?>    
    </body>
</html>
