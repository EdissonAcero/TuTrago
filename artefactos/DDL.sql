-- MySQL Script generated by MySQL Workbench
-- Sun Nov 29 16:11:13 2020
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema tu_tragobd
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema tu_tragobd
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `tu_tragobd` DEFAULT CHARACTER SET utf8 ;
USE `tu_tragobd` ;

-- -----------------------------------------------------
-- Table `tu_tragobd`.`Tipo_licor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tu_tragobd`.`Tipo_licor` (
  `idTipo_licor` INT NOT NULL AUTO_INCREMENT,
  `tipo` VARCHAR(45) NULL,
  PRIMARY KEY (`idTipo_licor`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tu_tragobd`.`licores`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tu_tragobd`.`licores` (
  `id_licores` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `valor_unidad` INT NOT NULL,
  `cantidad` INT NOT NULL,
  `Tipo_licor_idTipo_licor` INT NOT NULL,
  PRIMARY KEY (`id_licores`, `Tipo_licor_idTipo_licor`),
  INDEX `fk_licores_Tipo_licor1_idx` (`Tipo_licor_idTipo_licor` ASC) VISIBLE,
  CONSTRAINT `fk_licores_Tipo_licor1`
    FOREIGN KEY (`Tipo_licor_idTipo_licor`)
    REFERENCES `tu_tragobd`.`Tipo_licor` (`idTipo_licor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tu_tragobd`.`Telefono`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tu_tragobd`.`Telefono` (
  `idTelefono` INT NOT NULL AUTO_INCREMENT,
  `telefono` INT NOT NULL,
  PRIMARY KEY (`idTelefono`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tu_tragobd`.`Cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tu_tragobd`.`Cliente` (
  `idUsuario` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `apellido` VARCHAR(45) NOT NULL,
  `dirección` VARCHAR(45) NOT NULL,
  `NIT/CC` VARCHAR(45) NOT NULL,
  `correo` VARCHAR(45) NOT NULL,
  `clave` VARCHAR(50) NOT NULL,
  `Telefono_idTelefono` INT NOT NULL,
  PRIMARY KEY (`idUsuario`, `Telefono_idTelefono`),
  INDEX `fk_Usuario_Telefono1_idx` (`Telefono_idTelefono` ASC) VISIBLE,
  CONSTRAINT `fk_Usuario_Telefono1`
    FOREIGN KEY (`Telefono_idTelefono`)
    REFERENCES `tu_tragobd`.`Telefono` (`idTelefono`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tu_tragobd`.`Administrador`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tu_tragobd`.`Administrador` (
  `idAdministrador` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `apellido` VARCHAR(45) NOT NULL,
  `correo` VARCHAR(45) NOT NULL,
  `clave` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idAdministrador`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tu_tragobd`.`log_administrador`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tu_tragobd`.`log_administrador` (
  `idlog_admin` INT NOT NULL AUTO_INCREMENT,
  `accion` VARCHAR(45) NOT NULL,
  `datos` VARCHAR(45) NOT NULL,
  `fecha` VARCHAR(45) NOT NULL,
  `hora` VARCHAR(45) NOT NULL,
  `Administrador_idAdministrador` INT NOT NULL,
  PRIMARY KEY (`idlog_admin`, `Administrador_idAdministrador`),
  INDEX `fk_log_administrador_Administrador_idx` (`Administrador_idAdministrador` ASC) VISIBLE,
  CONSTRAINT `fk_log_administrador_Administrador`
    FOREIGN KEY (`Administrador_idAdministrador`)
    REFERENCES `tu_tragobd`.`Administrador` (`idAdministrador`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tu_tragobd`.`Telefono_domiciliario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tu_tragobd`.`Telefono_domiciliario` (
  `idTelefono` INT NOT NULL AUTO_INCREMENT,
  `telefono` INT NOT NULL,
  PRIMARY KEY (`idTelefono`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tu_tragobd`.`Domiciliario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tu_tragobd`.`Domiciliario` (
  `id_domiciliario` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `apellido` VARCHAR(45) NOT NULL,
  `direccion` VARCHAR(45) NOT NULL,
  `NIT/CC` INT NOT NULL,
  `correo` VARCHAR(45) NOT NULL,
  `clave` VARCHAR(45) NOT NULL,
  `Telefono_domiciliario_idTelefono` INT NOT NULL,
  PRIMARY KEY (`id_domiciliario`, `Telefono_domiciliario_idTelefono`),
  INDEX `fk_Domiciliario_Telefono_domiciliario1_idx` (`Telefono_domiciliario_idTelefono` ASC) VISIBLE,
  CONSTRAINT `fk_Domiciliario_Telefono_domiciliario1`
    FOREIGN KEY (`Telefono_domiciliario_idTelefono`)
    REFERENCES `tu_tragobd`.`Telefono_domiciliario` (`idTelefono`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tu_tragobd`.`Carrito_licores`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tu_tragobd`.`Carrito_licores` (
  `idCarrito_licores` INT NOT NULL AUTO_INCREMENT,
  `fecha` VARCHAR(45) NULL,
  `cantidad` INT NULL,
  `valor_total` INT NULL,
  `licores_id_licores` INT NOT NULL,
  `Domiciliario_id_domiciliario` INT NOT NULL,
  PRIMARY KEY (`idCarrito_licores`, `licores_id_licores`, `Domiciliario_id_domiciliario`),
  INDEX `fk_Carrito_licores_licores1_idx` (`licores_id_licores` ASC) VISIBLE,
  INDEX `fk_Carrito_licores_Domiciliario1_idx` (`Domiciliario_id_domiciliario` ASC) VISIBLE,
  CONSTRAINT `fk_Carrito_licores_licores1`
    FOREIGN KEY (`licores_id_licores`)
    REFERENCES `tu_tragobd`.`licores` (`id_licores`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Carrito_licores_Domiciliario1`
    FOREIGN KEY (`Domiciliario_id_domiciliario`)
    REFERENCES `tu_tragobd`.`Domiciliario` (`id_domiciliario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tu_tragobd`.`Carrito`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tu_tragobd`.`Carrito` (
  `idCarrito` INT NOT NULL AUTO_INCREMENT,
  `Cliente_idCliente` INT NOT NULL,
  PRIMARY KEY (`idCarrito`, `Cliente_idCliente`),
  INDEX `fk_Carrito_Cliente1_idx` (`Cliente_idCliente` ASC) INVISIBLE,
  CONSTRAINT `fk_Carrito_Cliente1`
    FOREIGN KEY (`Cliente_idCliente`)
    REFERENCES `tu_tragobd`.`Cliente` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Carrito_Domiciliario`
    FOREIGN KEY ()
    REFERENCES `tu_tragobd`.`Carrito_licores` ()
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
